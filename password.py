import os

print("Welcome to my very secure computer!")
print("To be able to login, you'll need a password.")

userPass = str(input("Please enter your password: "))

if "password" in userPass:
  userPass = userPass.replace("password","")

if userPass == "password":
  print("Password is correct, welcome back.")
  print("The token for this challenge is:", os.getenv("TOKEN"))
else:
  print("Password is not correct, try again.")